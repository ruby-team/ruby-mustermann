ruby = RbConfig::CONFIG['ruby_install_name']

Dir.chdir 'mustermann' do
  puts "cd mustermann"
  system("#{ruby} -S rspec --pattern ./spec/*_spec.rb -I ../support/lib")
end
