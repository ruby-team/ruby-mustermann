ruby-mustermann (3.0.0-2) unstable; urgency=medium

  * Team upload.
  * d/ruby-tests.rb: run mustermann tests.
  * d/control: add ruby-rspec{,-its} and ruby-sinatra as test dependencies.
  * Add patch to remove test require to avoid extra dependencies.
  * Add patch to skip tests that rely on mustermann-contrib code.

 -- Lucas Kanashiro <kanashiro@debian.org>  Fri, 16 Sep 2022 17:14:45 -0300

ruby-mustermann (3.0.0-1) unstable; urgency=medium

  * Team upload.

  [ Cédric Boutillier ]
  * [ci skip] Update team name

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

  [ Antonio Terceiro ]
  * debian/watch: point to tags page on github
  * New upstream version 3.0.0
  * Bump Standards-Version to 4.6.1; no changes needed
  * Refresh patches
  * Install mustermann/README.md instead of README.md.
    The top-level README.md just describe the repository organization, and
    provides no useful information for users.
  * Declare Breaks: against incompatible version of ruby-sinatra

 -- Antonio Terceiro <terceiro@debian.org>  Sat, 20 Aug 2022 13:29:30 -0300

ruby-mustermann (1.1.1-1) unstable; urgency=medium

  * Team upload

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.

  [ Antonio Terceiro ]
  * debian/watch: get source from github
  * New upstream version 1.1.1
  * Refresh packaging files
  * Refresh patches
  * Add debian/gemspec to fix dependency resolution for tests
  * Reduce warnings under ruby2.7
  * Add new build dependency: ruby-ruby2-keywords

 -- Antonio Terceiro <terceiro@debian.org>  Fri, 10 Apr 2020 17:15:04 -0300

ruby-mustermann (1.0.0-4) unstable; urgency=medium

  * Team upload
  * Fix version in Breaks/Replaces ruby-mustermann19 to cover version in
    stretch (Closes: #868851)
  * Use https for watch file and copyright format URL
  * Bump Standards-Version to 4.1.3 (no changes needed)
  * Bump debhelper compatibility to 11

 -- Cédric Boutillier <boutil@debian.org>  Wed, 14 Mar 2018 13:36:46 +0100

ruby-mustermann (1.0.0-3) unstable; urgency=medium

  * Team upload.
  * debian/rules: fix override_dh_auto_clean to work when patches are not
    applied yet (Closes: #870720)

 -- Antonio Terceiro <terceiro@debian.org>  Fri, 04 Aug 2017 12:25:44 -0300

ruby-mustermann (1.0.0-2) unstable; urgency=medium

  * Fix version in gemspec (Thanks to Antonio)

 -- Pirate Praveen <praveen@debian.org>  Mon, 03 Jul 2017 16:02:57 +0530

ruby-mustermann (1.0.0-1) unstable; urgency=medium

  * Initial release (Closes: #866205)

 -- Pirate Praveen <praveen@debian.org>  Wed, 28 Jun 2017 17:29:18 +0530
